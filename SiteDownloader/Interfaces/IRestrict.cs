﻿using System;

namespace SiteDownloader.Interfaces
{
	public interface IRestrict
	{
		RestrictTypes RestrictType { get; }
		bool IsAcceptable(Uri uri);
	}
}