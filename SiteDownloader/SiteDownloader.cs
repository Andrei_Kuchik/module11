﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using HtmlAgilityPack;
using SiteDownloader.Interfaces;

namespace SiteDownloader
{
	public class SiteDownloader : ISiteDownloader
	{
		private readonly ILogger _logger;
		private readonly IContentSaver _contentSaver;
		private readonly List<IRestrict> _urlRestricts;
		private readonly List<IRestrict> _fileRestricts;
		private const string HtmlDocumentMediaType = "text/html";
		private readonly ISet<Uri> _visitedUrls = new HashSet<Uri>();

		public int MaxDeepLevel { get; set; }

		public SiteDownloader(ILogger logger, IContentSaver contentSaver, IEnumerable<IRestrict> restricts,
			int maxDeepLevel)
		{
			if (maxDeepLevel < 0)
			{
				throw new ArgumentException($"{nameof(maxDeepLevel)} cannot be less than zero");
			}

			_logger = logger;
			_contentSaver = contentSaver;
			_urlRestricts = restricts.Where(c => (c.RestrictType & RestrictTypes.UrlRestrict) != 0).ToList();
			_fileRestricts = restricts.Where(c => (c.RestrictType & RestrictTypes.FileRestrict) != 0).ToList();

			MaxDeepLevel = maxDeepLevel;
		}

		public void LoadFromUrl(string url)
		{
			_visitedUrls.Clear();
			using (var httpClient = new HttpClient())
			{
				httpClient.BaseAddress = new Uri(url);
				ScanUrl(httpClient, httpClient.BaseAddress, 0);
			}
		}

		private void ScanUrl(HttpClient httpClient, Uri uri, int level)
		{
			if (level > MaxDeepLevel || _visitedUrls.Contains(uri) || !IsValidScheme(uri.Scheme))
			{
				return;
			}

			_visitedUrls.Add(uri);
			var request = new HttpRequestMessage(HttpMethod.Get, uri);
			_logger.Log($"Start Get request to {uri.AbsolutePath}");
			var response = httpClient.SendAsync(request);

			if (!response.Result.IsSuccessStatusCode)
			{
				return;
			}

			if (response.Result.Content.Headers.ContentType?.MediaType == HtmlDocumentMediaType)
			{
				ProcessHtmlDocument(httpClient, response.Result, uri, level);
			}
			else
			{
				ProcessFile(response.Result, uri);
			}
		}

		private void ProcessFile(HttpResponseMessage response, Uri uri)
		{
			_logger.Log($"File founded: {uri}");
			if (!IsAcceptableUri(uri, _fileRestricts))
			{
				return;
			}

			_logger.Log($"File loaded: {uri}");
			_contentSaver.SaveFile(uri, response.Content.ReadAsStreamAsync().Result);
		}

		private void ProcessHtmlDocument(HttpClient httpClient, HttpResponseMessage response, Uri uri, int level)
		{
			_logger.Log($"Url founded: {uri}");
			if (!IsAcceptableUri(uri, _urlRestricts))
			{
				return;
			}

			var document = new HtmlDocument();
			document.Load(response.Content.ReadAsStreamAsync().Result, Encoding.UTF8);
			_logger.Log($"Html loaded: {uri}");
			_contentSaver.SaveHtmlDocument(uri, GetDocumentFileName(document), GetDocumentStream(document));

			var attributesWithLinks = document.DocumentNode.Descendants()
				.SelectMany(d => d.Attributes.Where(IsAttributeWithLink));
			foreach (var attributesWithLink in attributesWithLinks)
			{
				ScanUrl(httpClient, new Uri(httpClient.BaseAddress, attributesWithLink.Value), level + 1);
			}
		}

		private string GetDocumentFileName(HtmlDocument document)
		{
			return document.DocumentNode.Descendants("title").FirstOrDefault()?.InnerText + ".html";
		}

		private Stream GetDocumentStream(HtmlDocument document)
		{
			var memoryStream = new MemoryStream();
			document.Save(memoryStream);
			memoryStream.Seek(0, SeekOrigin.Begin);
			return memoryStream;
		}

		private bool IsValidScheme(string scheme)
		{
			switch (scheme)
			{
				case "http":
				case "https":
					return true;
				default:
					return false;
			}
		}

		private bool IsAttributeWithLink(HtmlAttribute attribute)
		{
			return attribute.Name == "src" || attribute.Name == "href";
		}

		private bool IsAcceptableUri(Uri uri, IEnumerable<IRestrict> constraints)
		{
			return constraints.All(c => c.IsAcceptable(uri));
		}
	}
}