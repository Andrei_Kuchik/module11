﻿using System;

namespace SiteDownloader
{
	[Flags]
	public enum RestrictTypes
	{
		FileRestrict = 1,
		UrlRestrict = 2
	}
}