﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Module11.Enums;
using Module11.Restrictions;
using SiteDownloader.Interfaces;

namespace Module11
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var options = new Options("https://translate.google.com/", "D:/Site", deepLevel: 1,
				crossDomainTransfer: CrossDomainTransfer.DescendantUrlsOnly);
			IContentSaver contentSaver = new ContentSaver(new DirectoryInfo(options.PathToFolder));
			ILogger logger = new CustomLogger(options.IsLogging);
			var Restricts = GetRestrictsFromOptions(options);
			ISiteDownloader downloader =
				new SiteDownloader.SiteDownloader(logger, contentSaver, Restricts, options.DeepLevel);

			try
			{
				downloader.LoadFromUrl(options.Url);
			}
			catch (Exception ex)
			{
				logger.Log($"Error during site downloading: {ex.Message}");
			}
		}

		public static List<IRestrict> GetRestrictsFromOptions(Options options)
		{
			var Restricts = new List<IRestrict>();
			if (options.AvailableExtensions != null)
			{
				Restricts.Add(new FilesTypeRestrictions(options.AvailableExtensions.Split(',').Select(e => "." + e)));
			}

			Restricts.Add(new TransferToDomainRestrictions(options.CrossDomainTransfer, new Uri(options.Url)));

			return Restricts;
		}
	}
}