﻿namespace Module11.Enums
{
	public enum CrossDomainTransfer
	{
		All = 1,
		CurrentDomainOnly = 2,
		DescendantUrlsOnly = 3
	}
}