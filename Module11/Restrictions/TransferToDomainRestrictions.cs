﻿using System;
using Module11.Enums;
using SiteDownloader;
using SiteDownloader.Interfaces;

namespace Module11.Restrictions
{
	public class TransferToDomainRestrictions : IRestrict
	{
		private readonly Uri _parentUri;
		private readonly CrossDomainTransfer _availableTransition;

		public TransferToDomainRestrictions(CrossDomainTransfer availableTransition, Uri parentUri)
		{
			switch (availableTransition)
			{
				case CrossDomainTransfer.All:
				case CrossDomainTransfer.CurrentDomainOnly:
				case CrossDomainTransfer.DescendantUrlsOnly:
					_availableTransition = availableTransition;
					_parentUri = parentUri;
					break;
				default:
					throw new ArgumentException($"Unknown transition type: {availableTransition}");
			}
		}

		public RestrictTypes RestrictType => RestrictTypes.UrlRestrict | RestrictTypes.FileRestrict;

		public bool IsAcceptable(Uri uri)
		{
			switch (_availableTransition)
			{
				case CrossDomainTransfer.All:
					return true;
				case CrossDomainTransfer.CurrentDomainOnly:
					if (_parentUri.DnsSafeHost == uri.DnsSafeHost)
					{
						return true;
					}

					break;
				case CrossDomainTransfer.DescendantUrlsOnly:
					if (_parentUri.IsBaseOf(uri))
					{
						return true;
					}

					break;
			}

			return false;
		}
	}
}