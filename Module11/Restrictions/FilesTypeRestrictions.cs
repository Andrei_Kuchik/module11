﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteDownloader;
using SiteDownloader.Interfaces;

namespace Module11.Restrictions
{
	public class FilesTypeRestrictions: IRestrict
	{
		private readonly IEnumerable<string> _acceptableExtensions;

		public FilesTypeRestrictions(IEnumerable<string> acceptableExtensions)
		{
			_acceptableExtensions = acceptableExtensions;
		}

		public RestrictTypes RestrictType => RestrictTypes.FileRestrict;

		public bool IsAcceptable(Uri uri)
		{
			var lastSegment = uri.Segments.Last();
			return _acceptableExtensions.Any(e => lastSegment.EndsWith(e));
		}
	}


}
