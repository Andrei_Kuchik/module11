﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module11.Enums;

namespace Module11
{
	public class Options
	{
		public Options(string url, string pathToFolder, CrossDomainTransfer crossDomainTransfer = CrossDomainTransfer.All, string availableExtensions = "pdf,css,js", int deepLevel = 0,
			bool isLogging = true)
		{
			Url = url;
			PathToFolder = pathToFolder;
			CrossDomainTransfer = crossDomainTransfer;
			AvailableExtensions = availableExtensions;
			DeepLevel = deepLevel;
			IsLogging = isLogging;
		}

		public string Url { get; set; }
		public string PathToFolder { get; set; }
		public CrossDomainTransfer CrossDomainTransfer { get; set; }

		/*List of extensions, example: pdf,css,js */
		public string AvailableExtensions { get; set; }

		public int DeepLevel { get; set; }

		public bool IsLogging { get; set; }

	}
}
