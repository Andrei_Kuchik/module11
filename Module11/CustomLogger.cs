﻿using System;
using SiteDownloader.Interfaces;

namespace Module11
{
	public class CustomLogger : ILogger
	{
		private readonly bool _isLogEnabled;

		public CustomLogger(bool isLogEnabled)
		{
			_isLogEnabled = isLogEnabled;
		}

		public void Log(string message)
		{
			if (_isLogEnabled)
			{
				Console.WriteLine(message);
			}
		}
	}
}